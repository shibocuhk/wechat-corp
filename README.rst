=============================
Wechatcorp
=============================

.. image:: https://badge.fury.io/py/wechat-corp.png
    :target: https://badge.fury.io/py/wechat-corp

.. image:: https://travis-ci.org/shibocuhk/wechat-corp.png?branch=master
    :target: https://travis-ci.org/shibocuhk/wechat-corp

wechat corp admin

Documentation
-------------

The full documentation is at https://wechat-corp.readthedocs.org.

Quickstart
----------

Install Wechatcorp::

    pip install wechat-corp

Then use it in a project::

    import wechat_corp

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements_test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
