from wechatpy.enterprise import WeChatClient
from wechatpy.session.redisstorage import RedisStorage
from redis import Redis
from django.conf import settings

redis_client = Redis.from_url('redis://127.0.0.1:6379/0')
session_interface = RedisStorage(
    redis_client,
    prefix="wechatpy"
)
wechat_client = WeChatClient(
    settings.WECHAT_CORP_ID,
    settings.WECHAT_CORP_SECRET,
    session_interface
)