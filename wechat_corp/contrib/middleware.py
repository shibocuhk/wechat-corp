import time
from importlib import import_module

from django.conf import settings
from django.contrib.sessions.middleware import SessionMiddleware
from django.utils.cache import patch_vary_headers
from django.utils.functional import SimpleLazyObject
from django.utils.http import cookie_date
from wechat_corp.contrib import auth

WECHAT_SESSION_COOKIE_NAME = getattr(settings, 'WECHAT_SESSION_COOKIE_NAME', 'wechat_session')


class WechatCorpSessionMiddleware(SessionMiddleware):
    def process_request(self, request):
        session_key = request.COOKIES.get(WECHAT_SESSION_COOKIE_NAME)
        request.wechat_session = self.SessionStore(session_key)
    
    def process_response(self, request, response):
        """
        If request.session was modified, or if the configuration is to save the
        session every time, save the changes and set a session cookie or delete
        the session cookie if the session has been emptied.
        """
        try:
            accessed = request.wechat_session.accessed
            modified = request.wechat_session.modified
            empty = request.session.is_empty()
        except AttributeError:
            pass
        else:
            # First check if we need to delete this cookie.
            # The session should be deleted only if the session is entirely empty
            if WECHAT_SESSION_COOKIE_NAME in request.COOKIES and empty:
                response.delete_cookie(WECHAT_SESSION_COOKIE_NAME,
                                       domain=settings.SESSION_COOKIE_DOMAIN)
            else:
                if accessed:
                    patch_vary_headers(response, ('Cookie',))
                if (modified or settings.SESSION_SAVE_EVERY_REQUEST) and not empty:
                    if request.wechat_session.get_expire_at_browser_close():
                        max_age = None
                        expires = None
                    else:
                        max_age = request.wechat_session.get_expiry_age()
                        expires_time = time.time() + max_age
                        expires = cookie_date(expires_time)
                    # Save the session data and refresh the client cookie.
                    # Skip session save for 500 responses, refs #3881.
                    if response.status_code != 500:
                        request.wechat_session.save()
                        response.set_cookie(WECHAT_SESSION_COOKIE_NAME,
                                            request.wechat_session.session_key, max_age=max_age,
                                            expires=expires, domain=settings.SESSION_COOKIE_DOMAIN,
                                            path=settings.SESSION_COOKIE_PATH,
                                            secure=settings.SESSION_COOKIE_SECURE or None,
                                            httponly=settings.SESSION_COOKIE_HTTPONLY or None)
        return response


class WechatCorpAuthenticationMiddleware(object):
    def process_request(self, request):
        request.wechat_user = SimpleLazyObject(lambda: get_user(request))


def get_user(request):
    if not hasattr(request, '_cached_wechat_user'):
        request._cached_wechat_user = auth.get_user(request)
    return request._cached_wechat_user
