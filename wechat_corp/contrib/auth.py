from pprint import pprint

from django.conf import settings
from django.contrib.auth import user_logged_in
from django.contrib.auth import user_logged_out
from django.contrib.messages.storage import session
from django.middleware.csrf import rotate_token
from django.utils.crypto import constant_time_compare
from wechat_corp.core.models import User

SESSION_KEY = '_auth_user_id'
BACKEND_SESSION_KEY = '_auth_user_backend'
HASH_SESSION_KEY = '_auth_user_hash'
REDIRECT_FIELD_NAME = 'next'


def _get_user_session_key(request):
    # This value in the session is always serialized to a string, so we need
    # to convert it back to Python whenever we access it.
    return User._meta.pk.to_python(request.wechat_session[SESSION_KEY])


def get_user(request):
    from django.contrib.auth.models import AnonymousUser
    user = None
    try:
        user_id = _get_user_session_key(request)
    except KeyError:
        pass
    else:
        try:
            user = User.objects.get(pk=user_id)
            if ('django.contrib.auth.middleware.SessionAuthenticationMiddleware' in settings.MIDDLEWARE_CLASSES
                and hasattr(user, 'get_session_auth_hash')):
                session_hash = request.wechat_session.get(HASH_SESSION_KEY)
                session_hash_verified = session_hash and constant_time_compare(
                    session_hash,
                    user.get_session_auth_hash()
                )
                if not session_hash_verified:
                    request.wechat_session.flush()
                    user = None
        
        except User.DoesNotExist:
            user = None
    return user or AnonymousUser()


def login(request, user):
    """
    Persist a user id and a backend in the request. This way a user doesn't
    have to reauthenticate on every request. Note that data set during
    the anonymous session is retained when the user logs in.
    """
    session_auth_hash = ''
    if user is None:
        user = request.wechat_user
    if hasattr(user, 'get_session_auth_hash'):
        session_auth_hash = user.get_session_auth_hash()
    
    if SESSION_KEY in request.wechat_session:
        if _get_user_session_key(request) != user.pk or (
                    session_auth_hash and
                        request.wechat_session.get(HASH_SESSION_KEY) != session_auth_hash):
            # To avoid reusing another user's session, create a new, empty
            # session if the existing session corresponds to a different
            # authenticated user.
            request.wechat_session.flush()
    else:
        request.wechat_session.cycle_key()
    request.wechat_session[SESSION_KEY] = user._meta.pk.value_to_string(user)
    # request.wechat_session[BACKEND_SESSION_KEY] = user.backend
    request.wechat_session[HASH_SESSION_KEY] = session_auth_hash
    # if hasattr(request, 'wechat_user'):
    request.wechat_user = user
    rotate_token(request)
    user_logged_in.send(sender=user.__class__, request=request, user=user)


def logout(request):
    """
    Removes the authenticated user's ID from the request and flushes their
    session data.
    """
    # Dispatch the signal before the user is logged out so the receivers have a
    # chance to find out *who* logged out.
    user = getattr(request, 'wechat_user', None)
    if hasattr(user, 'is_authenticated') and not user.is_authenticated():
        user = None
    
    user_logged_out.send(sender=user.__class__, request=request, user=user)
    
    request.wechat_session.flush()
    
    if hasattr(request, 'wechat_user'):
        from django.contrib.auth.models import AnonymousUser
        request.wechat_user = AnonymousUser()
