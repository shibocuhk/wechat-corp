from pprint import pprint

from django.contrib import messages
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import JsonResponse
from django.shortcuts import redirect
from django.views.decorators.http import require_GET
from django.views.generic import RedirectView
from django.views.generic import TemplateView
from django.views.generic import CreateView
from django.views.generic import View
from wechat_corp.contrib.auth import login, logout

from wechat_corp.core.models import Department, User

from wechat_corp.mobile.forms.onboard_form import OnboardForm


class OnboardView(CreateView):
    template_name = 'mobile/onboard.html'
    form_class = OnboardForm
    success_url = reverse_lazy('wechat:mobile:onboard-success')
    
    def get(self, request, *args, **kwargs):
        if request.wechat_user.is_authenticated():
            return redirect('wechat:mobile:onboard-success')
        return super(OnboardView, self).get(request, *args, **kwargs)
    
    def form_valid(self, form):
        response = super(OnboardView, self).form_valid(form)
        login(self.request, form.instance)
        return response


class LogoutView(RedirectView):
    url = reverse_lazy('wechat:mobile:onboard-form')
    
    def get(self, request, *args, **kwargs):
        logout(self.request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class DepartmentsView(View):
    def get(self, *args, **kwargs):
        departments = Department.objects.filter(parent_id=self.request.GET.get('hospital', ''))
        result = []
        for department in departments:
            result.append({'name': department.name, 'id': department.id})
        return JsonResponse(result, safe=False)
