from __future__ import absolute_import, unicode_literals

from django.apps import AppConfig
from django.utils.translation import pgettext_lazy


class MobileAppConfig(AppConfig):
    name = 'wechat_corp.mobile'
    label = 'wechat_corp_mobile'
    verbose_name = pgettext_lazy('wechat_corp.apps', "Mobile")

    def ready(self):
        pass
