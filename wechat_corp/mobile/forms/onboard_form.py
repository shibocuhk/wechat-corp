from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, Div
from django import forms
from django.db.models import Q
from mptt.forms import TreeNodeChoiceField
from wechat_corp.core.fields import TreeNodeChoicePrefixField
from wechat_corp.core.models import User, Department
from django.utils.translation import pgettext_lazy


class OnboardForm(forms.ModelForm):
    parent = forms.CharField(widget=forms.widgets.Select,
                             help_text=pgettext_lazy('forms.onboard', 'Please select your department'),
                             label=pgettext_lazy('onboard_form', 'Department'))
    wechat_id = forms.CharField(required=True, label=pgettext_lazy('forms.onboard', 'WeChat ID'),
                                help_text=pgettext_lazy('forms.onboard',
                                                        'Open WeChat and get your wechat id in "Me" tab'))
    hospital = forms.ModelChoiceField(queryset=Department.objects.filter(Q(type=Department.TYPES.hospital)),
                                      help_text=pgettext_lazy('forms.onboard', 'Please select your hospital'),
                                      required=True,
                                      label=pgettext_lazy('forms.onboard', 'Hospital'))
    user_id = forms.CharField(required=True, label=pgettext_lazy('forms.onboard', 'Username'),
                              help_text=pgettext_lazy('forms.onboard', '5 to 20 alphanumeric characters'))
    
    def __init__(self, *args, **kwargs):
        super(OnboardForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'onboard_form'
        self.helper.form_id = 'onboard_form'
        self.helper.layout = Layout(
            Field('user_id'),
            Field('wechat_id'),
            Div(Field('hospital', css_class='js-hospital'), Field('parent', css_class='js-department'),
                css_id='dropdowns'),
            Field('name'),
            Field('email'),
            Field('phone'),
            Submit('submit', pgettext_lazy('onboard_form', 'submit')),
        )
    
    def clean_email(self):
        return self.cleaned_data['email'] or None
    
    def clean_phone(self):
        return self.cleaned_data['phone'] or None
    
    def clean_wechat_id(self):
        return self.cleaned_data['wechat_id'] or None
    
    def clean_parent(self):
        return Department.objects.get(id=self.cleaned_data['parent']) or None
    
    class Meta:
        model = User
        fields = ('name', 'user_id', 'parent', 'wechat_id', 'email', 'phone')
