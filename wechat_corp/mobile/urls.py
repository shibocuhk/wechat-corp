# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.views.generic import TemplateView
from wechat_corp.mobile.views import OnboardView, DepartmentsView, LogoutView

urlpatterns = []
urlpatterns += [
    url(r'^onboard/success/', TemplateView.as_view(template_name='mobile/success.html'), name='onboard-success'),
    url(r'^departments/', DepartmentsView.as_view(), name='departments'),
    url(r'^logout', LogoutView.as_view(), name='logout'),
    url(r'^onboard/', OnboardView.as_view(), name='onboard-form'),
]
