# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.views.generic import TemplateView
from wechat_corp.api import views

urlpatterns = [
    url(r'^callback', views.WeChatCallback.as_view())
]
