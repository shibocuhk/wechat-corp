# -- coding: utf-8 --
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from wechat_corp.contrib.service.wechat_corp_service import wechat_client
from wechat_corp.core.models import User, Chatroom
from wechat_corp.core.tasks import sync_user
from wechatpy.enterprise import WeChatCrypto
from wechatpy.enterprise import parse_message
from wechatpy.enterprise.exceptions import InvalidCorpIdException
from wechatpy.exceptions import InvalidSignatureException
from pprint import pprint
from django.http.response import JsonResponse, HttpResponse


class WeChatCallback(View):
    def __init__(self, *args, **kwargs):
        self.crypto = WeChatCrypto(settings.WECHAT_CORP_TOKEN, settings.WECHAT_CORP_AES, settings.WECHAT_CORP_ID)
        super(WeChatCallback, self).__init__(*args, **kwargs)
    
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(WeChatCallback, self).dispatch(request, *args, **kwargs)
    
    def get(self, *args, **kwargs):
        try:
            echo_str = self.crypto.check_signature(
                self.request.GET.get('msg_signature', ''),
                self.request.GET.get('timestamp', ''),
                self.request.GET.get('nonce', ''),
                self.request.GET.get('echostr', '')
            )
        except InvalidSignatureException:
            raise  # 处理异常情况
        else:
            return HttpResponse(status=200, content=echo_str)
    
    def post(self, *args, **kwargs):
        
        try:
            decrypted_xml = self.crypto.decrypt_message(
                self.request.body,
                self.request.GET.get('msg_signature', ''),
                self.request.GET.get('timestamp', ''),
                self.request.GET.get('nonce', ''),
            )
        except (InvalidCorpIdException, InvalidSignatureException):
            # 处理异常或忽略
            raise
        else:
            msg = parse_message(decrypted_xml)
            if msg.event == 'subscribe':
                user_id = msg.source
                if User.objects.filter(user_id=user_id).exists():
                    user = User.objects.filter(user_id=user_id).first()
                else:
                    remote_user = wechat_client.user.get()
                    user = sync_user(remote_user['user_id'])[0]
                department = user.parent
                chatroom = Chatroom.objects.filter(name=department.name).first()
                if chatroom:
                    wechat_client.chat.send_text(sender='admin', receiver_type='group', receiver_id=chatroom.chat_id,
                                                 content=u'欢迎%s加入群组' % user.name)
        
        return HttpResponse(status=200)
