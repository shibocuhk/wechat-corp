from __future__ import absolute_import, unicode_literals

from django.apps import AppConfig
from django.utils.translation import pgettext_lazy as pgettext_lazy


class ApiAppConfig(AppConfig):
    name = 'wechat_corp.api'
    label = 'wechat_corp_api'
    verbose_name = pgettext_lazy('wechat_corp.apps', "Api")
    
    def ready(self):
        pass
