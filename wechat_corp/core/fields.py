from django.utils.encoding import smart_text
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from mptt.forms import TreeNodeChoiceField


class TreeNodeChoicePrefixField(TreeNodeChoiceField):
    def label_from_instance(self, obj):
        return mark_safe(conditional_escape(smart_text(obj.parent) + ':' + smart_text(obj)))
