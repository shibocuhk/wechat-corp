from django.contrib import admin
from django import forms
from django_mptt_admin.admin import DjangoMpttAdmin
from mptt.admin import MPTTModelAdmin, DraggableMPTTAdmin
from wechat_corp.core.models import Department, User, Chatroom


class UserForm(forms.ModelForm):
    def clean_email(self):
        return self.cleaned_data['email'] or None
    
    def clean_phone(self):
        return self.cleaned_data['phone'] or None
    
    def clean_wechat_id(self):
        return self.cleaned_data['wechat_id'] or None
    
    class Meta:
        model = User
        fields = ('name', 'parent', 'wechat_id', 'email', 'phone')


@admin.register(Chatroom)
class ChatroomAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


@admin.register(Department)
class DepartmentMPTTModelAdmin(DjangoMpttAdmin):
    # specify pixel amount for this ModelAdmin only:
    mptt_level_indent = 20
    fields = ('name', 'parent', 'type')


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_select_related = ['parent']
    list_display = ('id', 'name', 'user_id', 'email', 'wechat_id', 'phone', 'parent_name')
    fields = ('name', 'email', 'wechat_id', 'phone', 'parent')
    form = UserForm
    
    def parent_name(self, obj):
        if obj.parent:
            return obj.parent.name
        else:
            ''
