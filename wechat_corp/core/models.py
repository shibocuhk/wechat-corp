# -*- coding: utf-8 -*-
import uuid

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.postgres.fields import JSONField
from django.core.validators import RegexValidator, EmailValidator
from django.db import models
from django.utils.six import python_2_unicode_compatible
from django.utils.translation import pgettext_lazy
from model_utils import Choices
from model_utils import FieldTracker
from model_utils.models import TimeStampedModel
from mptt.fields import TreeForeignKey, TreeManyToManyField
from mptt.models import MPTTModel


@python_2_unicode_compatible
class Department(MPTTModel, TimeStampedModel):
    TYPES = Choices(("hospital", pgettext_lazy("model.department", "hospital")),
                    ("department", pgettext_lazy("model.department", "Department")),
                    ("admin", pgettext_lazy("model.department", "Admin")),
                    ("other", pgettext_lazy("model.department", "Other")))
    name = models.CharField(max_length=250, blank=False)
    name_english = models.CharField(max_length=250, blank=True)
    remote_id = models.IntegerField(blank=True, default=None, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    order = models.IntegerField(blank=True, null=True)
    type = models.CharField(choices=TYPES, default=TYPES.hospital, max_length=100)
    tracker = FieldTracker()
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = pgettext_lazy('model.department', 'Department')
        db_table = 'wechat_corp_departments'


@python_2_unicode_compatible
class User(TimeStampedModel, AbstractBaseUser):
    status = models.CharField(
        choices=(
            ('pending', pgettext_lazy('model.user', 'Pending')), ('approved', pgettext_lazy('model.user', 'Approved')),
            ('rejected', pgettext_lazy('model.user', 'Rejected'))),
        max_length=100,
        default='pending', verbose_name=pgettext_lazy('model.user', 'Status'))
    name = models.CharField(max_length=250, blank=False, verbose_name=pgettext_lazy('model.user', 'Name'))
    position = models.CharField(max_length=250, blank=True, verbose_name=pgettext_lazy('model.user', 'Position'))
    user_id = models.CharField(max_length=250, blank=False, verbose_name=pgettext_lazy('mode.user', 'Username'),
                               unique=True,
                               validators=[
                                   RegexValidator(regex=r'^[a-zA-Z][a-zA-Z0-9_]+$', message=pgettext_lazy('model.user',
                                                                                                          'Username should only contain alphabet, numbers, underscore, and start with alphabet')),
                                   RegexValidator(regex=r'.{5,20}', message=pgettext_lazy('model.user',
                                                                                          'Username should be a string contains 5-20 characters'))
                               ])
    wechat_id = models.CharField(max_length=250, blank=True, db_index=True, unique=True, null=True,
                                 verbose_name=pgettext_lazy('model.user', 'WeChat Account'),
                                 validators=[
                                     RegexValidator(regex=r'^[a-zA-Z][a-zA-Z0-9_]+$',
                                                    message=pgettext_lazy('model.user',
                                                                          'Wechat ID  should only contain alphabet, numbers, underscore, and start with alphabet')),
                                     RegexValidator(regex=r'.{5,20}', message=pgettext_lazy('model.user',
                                                                                            'Wechat ID should be a string contains 5-20 characters'))
                                 ]
                                 )
    phone = models.CharField(validators=[RegexValidator(regex=r'.*'), ], blank=True, null=True, max_length=100,
                             unique=True,
                             verbose_name=pgettext_lazy('model.user', 'Phone'))
    email = models.CharField(max_length=250, blank=False, validators=[EmailValidator, ], unique=True, null=True,
                             verbose_name=pgettext_lazy('model.user', 'Email'))
    gender = models.CharField(
        choices=(('m', pgettext_lazy('model.user', 'Male')), ('f', pgettext_lazy('model.user', 'Female'))), blank=True,
        max_length=10, verbose_name=pgettext_lazy('model.user', 'Gender'))
    name_card = models.ImageField(upload_to='name_card', blank=True,
                                  verbose_name=pgettext_lazy('model.user', 'NameCard'))
    wechat_info = JSONField(blank=True, default={})
    extrattr = JSONField(blank=True, default={})
    avatar = models.URLField(blank=True)
    platform_status = models.CharField(choices=(
        ('1', pgettext_lazy('model.user.platform_status', 'followed')),
        ('2', pgettext_lazy('model.user.platform_status', 'disabled')),
        ('4', pgettext_lazy('model.user.platform_status', 'unfollowed'))),
        max_length=5)
    parent = TreeForeignKey('Department', null=True, blank=True, related_name='users', db_index=True,
                            on_delete=models.SET_NULL)
    tracker = FieldTracker()
    
    def __init__(self, *args, **kwargs):
        super(AbstractBaseUser, self).__init__(*args, **kwargs)
        # Stores the raw password if set_password() is called so that it can
        # be passed to password_changed() after the model is saved.
        self._password = None
        if not self.password:
            self.set_unusable_password()
    
    def __str__(self):
        return self.name
    
    def get_username(self):
        return self.user_id
    
    class Meta:
        verbose_name = pgettext_lazy('model.user', 'User')
        db_table = 'wechat_corp_users'


User._meta.get_field('password').blank = True


def get_chat_id():
    return uuid.uuid4().hex


@python_2_unicode_compatible
class Chatroom(TimeStampedModel):
    name = models.CharField(max_length=250, blank=False)
    users = models.ManyToManyField(to=User, related_name='chatrooms')
    owner = models.ForeignKey(to=User, related_name='owned_chatrooms', blank=True, null=True)
    chat_id = models.CharField(max_length=32, default=get_chat_id, blank=True)
    tracker = FieldTracker()
    
    def __init__(self, *args, **kwargs):
        super(Chatroom, self).__init__(*args, **kwargs)
    
    def __str__(self):
        return self.name
    
    class Meta:
        db_table = 'wechat_corp_chatrooms'
        verbose_name = pgettext_lazy('model.chatroom', 'Chatroom')


@python_2_unicode_compatible
class App(TimeStampedModel):
    name = models.CharField(max_length=250, blank=False)
    agent_id = models.IntegerField(blank=True)
    description = models.TextField(blank=True)
    square_logo_url = models.URLField(blank=True)
    round_logo_url = models.URLField(blank=True)
    allowed_users = models.ManyToManyField('User')
    allowed_parties = models.ManyToManyField('Department')
    close = models.BooleanField(default=True)
    redirect_domain = models.CharField(max_length=250, blank=True)
    report_location_flag = models.BooleanField(default=True)
    is_report_user = models.BooleanField(default=True)
    is_report_enter = models.BooleanField(default=True)
    type = models.CharField(max_length=5,
                            choices=(('1', pgettext_lazy('model.app.type', 'message')),
                                     ('2', pgettext_lazy('model.app.type', 'page'))))
    chat_extension_url = models.URLField(blank=True)
    menus = JSONField()
    
    tracker = FieldTracker()
    
    def __str__(self):
        return self.name
    
    class Meta:
        db_table = 'wechat_corp_apps'
        verbose_name = pgettext_lazy('model.app', 'App')
