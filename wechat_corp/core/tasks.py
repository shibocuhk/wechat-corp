# -- coding: utf-8 --
import datetime
from celery.task import periodic_task
from django.db.models.signals import post_save, post_delete
from django.db import transaction
from wechat_corp.contrib.service.wechat_corp_service import wechat_client
from wechat_corp.core.models import Department, User
from signals import update_user, create_department, delete_department, delete_user
import re

class NoHospitalException(Exception):
    pass


@periodic_task(run_every=datetime.timedelta(minutes=2))
def sync_contact():
    sync_department()
    sync_user()


def sync_user(user_id=None):
    db_users = []
    if user_id:
        users = [wechat_client.user.get(user_id)]
    else:
        users = wechat_client.user.list(department_id=1, fetch_child=True)
    post_save.disconnect(update_user, sender=User)
    post_delete.disconnect(delete_user, sender=User)
    for user in users:
        db_user = User.objects.filter(user_id=user['userid']).first()
        if db_user:
            pass
        else:
            db_user = User()
            db_user.user_id = user['userid']
        db_user.name = user['name']
        db_user.position = user.get('position', '')
        db_user.phone = user.get('mobile', None)
        db_user.email = user.get('email', None)
        if user['gender'] == 1:
            db_user.gender = 'm'
        elif user['gender'] == 2:
            db_user.gender = 'f'
        else:
            db_user.gender = ''
        db_user.wechat_id = user.get('weixinid', None)
        db_user.avatar = user.get('avatar', '')
        db_user.status = str(user['status'])
        db_user.extattr = user.get('extattr', {})
        department = Department.objects.filter(remote_id=user['department'][-1]).first()
        db_user.parent = department
        db_user.save()
        db_users.append(db_user)
    User.objects.exclude(user_id__in=[u['userid'] for u in users]).delete()
    post_save.connect(update_user, sender=User)
    post_delete.connect(delete_user, sender=User)
    return db_users


def sync_department():
    departments = wechat_client.department.get()
    hospital_root = next((x for x in departments if x['name'] == u'医院(Hospitals)'), None)
    db_departments = {}
    post_save.disconnect(create_department, sender=Department)
    post_delete.disconnect(delete_department, sender=Department)
    if not hospital_root:
        raise NoHospitalException("No Hospital Root Found")
    else:
        with Department.objects.disable_mptt_updates():
            for department in departments:
                if department['parentid'] == hospital_root['id']:
                    department_type = Department.TYPES.hospital
                elif department['id'] == hospital_root['id']:
                    department_type = Department.TYPES.admin
                elif department['parentid'] == 1 or department['id'] == 1:
                    department_type = Department.TYPES.admin
                else:
                    department_type = Department.TYPES.department
                
                db_department = Department.objects.filter(remote_id=department['id']).first()
                if db_department:
                    db_department.remote_id = department['id']
                    db_department.name = department['name']
                    db_department.type = department_type
                    db_department.order = department['order']
                    db_department.save()
                else:
                    db_department = Department.objects.create(name=department['name'], type=department_type,
                                                              remote_id=department['id'],
                                                              parent=None,
                                                              order=department['order'])
                db_departments[department['id']] = db_department
            
            for department in departments:
                db_department = db_departments[department['id']]
                if department['parentid'] != 0:
                    db_department.parent = db_departments[department['parentid']]
                else:
                    db_department.parent = None
                db_department.save()
            Department.objects.exclude(remote_id__in=[d['id'] for d in departments]).delete()
        Department.objects.rebuild()
    post_save.connect(create_department, sender=Department)
    post_delete.connect(delete_department, sender=Department)
