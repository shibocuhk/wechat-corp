# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^mobile/', include('wechat_corp.mobile.urls', namespace='mobile')),
    url(r'^api/', include('wechat_corp.api.urls', namespace='api'))
]
