from __future__ import absolute_import, unicode_literals

from django.apps import AppConfig
from django.utils.translation import pgettext_lazy


class CoreAppConfig(AppConfig):
    name = 'wechat_corp.core'
    label = 'wechat_corp_core'
    verbose_name = pgettext_lazy('wechat_corp.apps', "WeChat Corp")
    
    def ready(self):
        import wechat_corp.core.signals
        pass
