from collections import defaultdict
from pprint import pprint
from django.db.models.signals import post_save, pre_save, post_delete, post_init, pre_delete, m2m_changed, post_migrate, \
    pre_migrate, pre_init
from django.dispatch import receiver
from wechat_corp.contrib.service.wechat_corp_service import wechat_client
from wechat_corp.core.models import App
from wechat_corp.core.models import Chatroom
from wechat_corp.core.models import Department
from wechat_corp.core.models import User
from wechatpy import WeChatClientException


class DisableSignals(object):
    def __init__(self, disabled_signals=None):
        self.stashed_signals = defaultdict(list)
        self.disabled_signals = disabled_signals or [
            pre_init, post_init,
            pre_save, post_save,
            pre_delete, post_delete,
            pre_migrate, post_migrate,
        ]
    
    def __enter__(self):
        for signal in self.disabled_signals:
            self.disconnect(signal)
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        for signal in self.stashed_signals.keys():
            self.reconnect(signal)
    
    def disconnect(self, signal):
        self.stashed_signals[signal] = signal.receivers
        signal.receivers = []
    
    def reconnect(self, signal):
        signal.receivers = self.stashed_signals.get(signal, [])
        del self.stashed_signals[signal]


@receiver(post_save, sender=App)
def create_app(sender, **kwargs):
    pass


@receiver(m2m_changed, sender=Chatroom.users.through)
def change_chatroom_users(sender, **kwargs):
    instance = kwargs['instance']
    action = kwargs['action']
    if action == 'post_add':
        pk_set = kwargs['pk_set']
        add_user_list = User.objects.filter(id__in=pk_set).values_list('user_id', flat=True)
        
        wechat_client.chat.update(chat_id=instance.chat_id, op_user='admin', add_user_list=add_user_list or None,
                                  owner=instance.owner.user_id if instance.owner else 'admin', )
    if action == 'post_remove':
        pk_set = kwargs['pk_set']
        del_user_list = User.objects.filter(id__in=pk_set).values_list('user_id', flat=True)
        
        wechat_client.chat.update(chat_id=instance.chat_id, op_user='admin', del_user_list=del_user_list or None,
                                  owner=instance.owner.user_id if instance.owner else 'admin', )


@receiver(post_save, sender=Chatroom)
def create_chatroom(sender, **kwargs):
    instance = kwargs['instance']
    created = kwargs['created']
    if created:
        wechat_client.chat.create(chat_id=instance.chat_id, name=instance.name, owner='admin',
                                  user_list=list(instance.users.values_list('user_id', flat=True)) + ['admin',
                                                                                                      'bruceshi',
                                                                                                      'petershi'])
    else:
        wechat_client.chat.update(chat_id=instance.chat_id, name=instance.name, op_user='admin')


@receiver(pre_delete, sender=Chatroom)
def delete_chatroom(sender, **kwargs):
    instance = kwargs['instance']
    wechat_client.chat.update(chat_id=instance.chat_id, op_user='admin',
                              del_user_list=instance.users.values_list('user_id') or None)


@receiver(post_save, sender=User)
def update_user(sender, **kwargs):
    instance = kwargs['instance']
    created = kwargs['created']
    
    if created:
        wechat_client.user.create(user_id=instance.user_id, name=instance.name, email=instance.email,
                                  weixin_id=instance.wechat_id, mobile=instance.phone,
                                  department=[instance.parent.remote_id] if instance.parent else None, )
    else:
        wechat_client.user.update(user_id=instance.user_id, name=instance.name, email=instance.email,
                                  weixin_id=instance.wechat_id, mobile=instance.phone,
                                  department=[instance.parent.remote_id] if instance.parent else None)


@receiver(post_save, sender=User)
def update_user_chatroom(sender, **kwargs):
    instance = kwargs['instance']
    created = kwargs['created']
    if instance.tracker.has_changed('parent_id'):
        pre_department = Department.objects.filter(id=instance.tracker.previous('parent_id')).first()
        department = instance.parent
        if pre_department:
            chatroom = Chatroom.objects.filter(name=pre_department.name).first()
            if chatroom:
                try:
                    wechat_client.chat.update(chat_id=chatroom.chat_id, op_user='admin',
                                              del_user_list=[instance.user_id])
                except WeChatClientException:
                    pass
        
        if department:
            chatroom = Chatroom.objects.filter(name=department.name).first()
            if chatroom:
                try:
                    wechat_client.chat.update(chat_id=chatroom.chat_id, op_user='admin',
                                              add_user_list=[instance.user_id])
                except WeChatClientException:
                    pass


@receiver(post_delete, sender=User)
def delete_user(sender, **kwargs):
    instance = kwargs['instance']
    wechat_client.user.delete(user_id=instance.user_id)


@receiver(post_save, sender=Department)
def create_department_chatroom(sender, **kwargs):
    instance = kwargs['instance']
    created = kwargs['created']
    if created:
        Chatroom.objects.get_or_create(name=instance.name,
                                       defaults={
                                           'owner': User.objects.filter(user_id='admin').first()
                                       })
    else:
        name = instance.tracker.previous('name')
        chatroom = Chatroom.objects.filter(name=name).first()
        if chatroom:
            chatroom.name = instance.name
            chatroom.save()
        else:
            Chatroom.objects.create(name=instance.name, owner=User.objects.filter(user_id='admin').first())


@receiver(post_save, sender=Department)
def create_department(sender, **kwargs):
    instance = kwargs['instance']
    if not instance.remote_id:
        try:
            data = wechat_client.department.create(parent_id=instance.parent.remote_id, order=instance.order,
                                                   name=instance.name)
        except Exception:
            pass
        Department.objects.filter(id=instance.id).update(remote_id=data['id'])
    else:
        wechat_client.department.update(id=instance.remote_id, parent_id=instance.parent.remote_id,
                                        order=instance.order, name=instance.name)


@receiver(post_delete, sender=Department)
def delete_department(sender, **kwargs):
    instance = kwargs['instance']
    wechat_client.department.delete(id=instance.remote_id)
