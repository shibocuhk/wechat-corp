============
Installation
============

At the command line::

    $ easy_install wechat-corp

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv wechat-corp
    $ pip install wechat-corp
